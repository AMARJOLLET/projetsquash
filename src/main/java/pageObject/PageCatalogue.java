package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Nav;

public class PageCatalogue extends Nav {
	public PageCatalogue(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy (xpath = "//a[text()='Sign In']")
	public WebElement signIn;
	
	@FindBy (id = "WelcomeContent")
	public WebElement welcome;
	
	@FindBy (xpath = "//a[text()='Sign Out']")
	public WebElement signOut;
	
}