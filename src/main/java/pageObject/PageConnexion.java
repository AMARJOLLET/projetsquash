package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Nav;

public class PageConnexion extends Nav {
	public PageConnexion(WebDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy (name = "username")
	public WebElement username;
	
	@FindBy (name = "password")
	public WebElement password;
	
	@FindBy (name = "signon")
	public WebElement login;
	
}