package step_definitions;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import pageObject.PageCatalogue;
import pageObject.PageConnexion;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StepDef {
    public static WebDriver driver;

    PageConnexion page_connexion;
    PageCatalogue page_catalogue;


    @Given("un navigateur est ouvert")
    public void un_navigateur_est_ouvert() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5L));
        driver.manage().window().maximize();
    }

    @Given("Je suis sur la page d'accueil de Jpetstore")
    public void jeSuisSurLaPageDAccueilDeJpetstore() {
        un_navigateur_est_ouvert();
        je_suis_sur_url();
    }

    @When("je suis sur url")
    public void je_suis_sur_url() {
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
    }

    @When("je clique sur le lien de connexion")
    public void je_clique_sur_le_lien_de_connexion() {
        page_catalogue = new PageCatalogue(driver);
        page_catalogue.signIn.click();
    }

    @When("rentre le Username {string}")
    public void rentre_le_Username(String username) {
        page_connexion = new PageConnexion(driver);
        page_connexion.username.clear();
        page_connexion.username.sendKeys(username);

    }

    @When("rentre le Password {string}")
    public void rentre_le_Password(String password) {
        page_connexion.password.clear();
        page_connexion.password.sendKeys(password);
    }

    @When("je clique sur login")
    public void je_clique_sur_login() {
        page_connexion.login.click();
    }

    @Then("utilisateur ABC est connecte")
    public void utilisateur_ABC_est_connecte() {
        assertEquals("Sign Out", page_catalogue.signOut.getText(), "Erreur sur la présence du bouton Sign Out");
    }

    @Then("je peux lire le message accueil {string}")
    public void je_peux_lire_le_message_accueil(String message) {
        assertEquals(message, page_catalogue.welcome.getText(), "Message d'accueil erroné");
    }

    @After
    public void tearDown() {
        driver.quit();
    }


}