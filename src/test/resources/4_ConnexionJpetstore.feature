# language: en
Feature: ConnexionJpetstore

	Scenario Outline: ConnexionJpetstore
		Given Je suis sur la page d'accueil de Jpetstore
		When je clique sur le lien de connexion
		And rentre le Username <user>
		And rentre le Password <pwd>
		And je clique sur login
		Then utilisateur ABC est connecte
		And je peux lire le message accueil <message>

		@ACID
		Examples:
		| message | pwd | user |
		| "Welcome ABC!" | "ACID" | "ACID" |

		@j2ee
		Examples:
		| message | pwd | user |
		| "Welcome ABC!" | "j2ee" | "j2ee" |